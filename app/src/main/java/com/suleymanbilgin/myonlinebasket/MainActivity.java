package com.suleymanbilgin.myonlinebasket;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;

import com.parse.Parse;
import com.suleymanbilgin.myonlinebasket.custom.BaseActivity;
import com.suleymanbilgin.myonlinebasket.fragment.AddItemFragment;
import com.suleymanbilgin.myonlinebasket.fragment.HomepageFragment;
import com.suleymanbilgin.myonlinebasket.fragment.ProductListFragment;

public class MainActivity extends BaseActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final long DRAWER_CLOSE_DELAY_MS = 250;
    private static final String NAV_ITEM_ID = "navItemId";

    private final Handler mDrawerActionHandler = new Handler();
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    private int mNavItemId;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Parse.enableLocalDatastore(this);
        Parse.initialize(this);
        setContentView(R.layout.activity_main);

        init();
        // load saved navigation state if present
        if (null == savedInstanceState) {
            mNavItemId = R.id.drawer_item_homepage_latest_posts;
        } else {
            mNavItemId = savedInstanceState.getInt(NAV_ITEM_ID);
        }

        drawerLayoutSetup();
    }

    public void init() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);

    }

    /**
     * This method creates the Navigation Drawer on left side. (Sliding Navigation Drawer)
     */
    public void drawerLayoutSetup() {
        // listen for navigation events
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation);
        navigationView.setNavigationItemSelectedListener(this);

        // select the correct nav menu item
        navigationView.getMenu().findItem(mNavItemId).setChecked(true);

        // set up the hamburger icon to open and close the drawer
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open,
                R.string.close);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        navigate(mNavItemId);
    }

    /**
     * This method can be used to attach Fragment on activity view for a
     * particular tab position. You can customize this method as per your need.
     *
     * @param itemId id's item of navigation drawer
     */
    private void navigate(final int itemId) {
        Fragment f = null;
        switch (itemId) {
            case R.id.drawer_item_homepage_latest_posts:
                f = new HomepageFragment();
                launchFragment(f, R.string.app_name);
                getSupportActionBar().setTitle(getString(R.string.app_name));
                break;
            case R.id.drawer_item_add_item:
                f = new AddItemFragment();
                launchFragment(f, R.string.fragment_add_item);
                getSupportActionBar().setTitle(getString(R.string.fragment_add_item));
                break;
            case R.id.drawer_item_product_list:
                f = new ProductListFragment();
                launchFragment(f, R.string.fragment_product_list);
                getSupportActionBar().setTitle(getString(R.string.fragment_product_list));
                break;
            case R.id.drawer_item_quit:
                finish();
                break;
            /*case R.id.drawer_item_message_from_mayor:
                f = new MessageFromMayorFragment();
                launchFragment(f, R.string.title_fragment_message_from_mayor);
                getSupportActionBar().setTitle(getString(R.string.message_from_mayor));
                break;
            case R.id.drawer_item_map:
                Intent maps_activity = new Intent(MainActivity.this, MapsActivity.class);
                startActivity(maps_activity);
                break;
            case R.id.drawer_item_settings:
                Intent settings_activity = new Intent(MainActivity.this, SettingsActivity.class);
                startActivity(settings_activity);
                break;*/
            default:

        }
    }

    /**
     * This method can be used to attach Fragment on activity view for a
     * particular tab position. You can customize this method as per your need.
     *
     * @param f        fragment of selected (you can see how to use in navigate method)
     * @param title_id id of fragment's title string
     */
    public void launchFragment(Fragment f, int title_id) {
        if (f != null) {
            while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            }
            getSupportFragmentManager().beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left,android.R.anim.slide_out_right)
                    .replace(R.id.content_frame, f).addToBackStack(getResources().getString(title_id))
                    .commit();
        }
    }

    @Override
    public boolean onNavigationItemSelected(final MenuItem menuItem) {
        // update highlighted item in the navigation menu
        menuItem.setChecked(true);
        mNavItemId = menuItem.getItemId();

        // allow some time after closing the drawer before performing real navigation
        // so the user can see what is happening
        mDrawerLayout.closeDrawer(GravityCompat.START);
        mDrawerActionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                navigate(menuItem.getItemId());
            }
        }, DRAWER_CLOSE_DELAY_MS);
        return true;
    }

    @Override
    public void onConfigurationChanged(final Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        switch (item.getItemId()) {
            case android.support.v7.appcompat.R.id.home:
                return mDrawerToggle.onOptionsItemSelected(item);

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(NAV_ITEM_ID, mNavItemId);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStackImmediate();
            } else
                finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    /*
    MENÜ EKLENECEK OLDUGU ZAMAN ACILABILIR

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */

}
