package com.suleymanbilgin.myonlinebasket;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.myonlinebasket.utils.Log;

import org.json.JSONObject;

/**
 * Created by Laptop on 15/09/2015.
 */
public class RegisterActivity extends AppCompatActivity {

    EditText tvUsername, tvPassword;
    Button btnRegister;

    Log log;
    RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();
        actions();

    }

    void init() {
        tvUsername = (EditText) findViewById(R.id.et_username);
        tvPassword = (EditText) findViewById(R.id.et_password);
        btnRegister = (Button) findViewById(R.id.btn_register);
        log = new Log();
        mRequestQueue = new Volley().newRequestQueue(getApplicationContext());
    }

    void actions() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( tvUsername.getText().length() < 4 && tvPassword.getText().length() < 4) {

                }
                else {
                    register();
                }
            }
        });
    }

    void register() {
        String url = getString(R.string.base_url) + getString(R.string.extension) + getString(R.string.user_register) + "?name=" + tvUsername.getText().toString() + "&password=" +  tvPassword.getText().toString();

        JsonObjectRequest json = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                log.log("calisti");
                Snackbar.make(findViewById(android.R.id.content), "Basariyla Kayit Olundu", Snackbar.LENGTH_LONG)
                        .show();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                log.log("hatali calisti");
                Snackbar.make(findViewById(android.R.id.content), "Basariyla Kayit Olundu", Snackbar.LENGTH_LONG)
                        .show();
            }
        });
        mRequestQueue.add(json);
    }
}
