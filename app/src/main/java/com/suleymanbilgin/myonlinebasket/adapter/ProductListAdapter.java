package com.suleymanbilgin.myonlinebasket.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.suleymanbilgin.myonlinebasket.R;

/**
 * Created by Laptop on 14/09/2015.
 */
public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    View view;

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        //return postsList == null ? 0 : postsList.size();
        return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvProductName, tvWantUser, tvProductCount;

        public ViewHolder(View itemView) {
            super(itemView);

        }
    }
}
