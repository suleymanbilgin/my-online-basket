package com.suleymanbilgin.myonlinebasket.custom;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.suleymanbilgin.myonlinebasket.utils.ConnectionDetector;

/**
 * This is a common activity that all other activities of the app can extend to
 * inherit the common behaviors like implementing a common interface that can be
 * used in all child activities.
 *
 * @author Promob Web and App Development
 * @since 1.0
 */
public class BaseActivity extends AppCompatActivity {

    ConnectionDetector cd;
    public RequestQueue mRequestQueue;
    public ProgressDialog mProgressBarDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(isCon()) {
            mRequestQueue = new Volley().newRequestQueue(getApplicationContext());
        }
        else {
            cd.makeAlert(BaseActivity.this);
        }
    }

    private Boolean isCon() {
        cd = new ConnectionDetector(this);
        return cd.isConnectingToInternet();
    }
}
