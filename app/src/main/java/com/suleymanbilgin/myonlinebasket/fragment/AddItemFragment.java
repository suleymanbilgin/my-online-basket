package com.suleymanbilgin.myonlinebasket.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.suleymanbilgin.myonlinebasket.R;
import com.suleymanbilgin.myonlinebasket.custom.BaseFragment;

/**
 * Created by Laptop on 14/09/2015.
 */
public class AddItemFragment extends BaseFragment {
    View view;
    TextInputLayout tilCount;
    CheckBox cbCount;
    Button btnAddProduct;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_add_product, container, false);

        init();
        actions();

        return view;
    }

    void init() {
        tilCount = (TextInputLayout) view.findViewById(R.id.til3);
        cbCount = (CheckBox) view.findViewById(R.id.cb_is_it_count);
        btnAddProduct = (Button) view.findViewById(R.id.btn_add_product);
    }

    void actions() {
        cbCount.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b == false){
                    tilCount.setVisibility(View.GONE);
                }
                else if (b == true) {
                    tilCount.setVisibility(View.VISIBLE);
                }
            }
        });
        btnAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(getActivity().findViewById(android.R.id.content), "ürün listeye eklendi", Snackbar.LENGTH_LONG)
                        .show();
            }
        });
    }
}
