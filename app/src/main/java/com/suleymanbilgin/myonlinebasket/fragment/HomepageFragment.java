package com.suleymanbilgin.myonlinebasket.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.suleymanbilgin.myonlinebasket.R;
import com.suleymanbilgin.myonlinebasket.custom.BaseFragment;

/**
 * Bu ekranda kullanici ilk girdiginde siteyle ilgili son bilgiler ya da mesaj ya da yeni alinmayi bekleyen bir sey varsa gosterilecek
 *
 * kisacasi genel bilgiler yer alacak
 *
 * Created by Laptop on 14/09/2015.
 */
public class HomepageFragment extends BaseFragment {
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_homepage, container, false);

        init();

        return view;
    }

    void init() {
        Snackbar.make(getActivity().findViewById(android.R.id.content), "Sepetinize Hosgeldiniz", Snackbar.LENGTH_LONG)
                .show();
    }
}
