package com.suleymanbilgin.myonlinebasket.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.suleymanbilgin.myonlinebasket.R;
import com.suleymanbilgin.myonlinebasket.custom.BaseFragment;

/**
 * Created by Laptop on 14/09/2015.
 */
public class ProductListFragment extends BaseFragment {

    View view;
    RecyclerView rvProducts;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_products, container, false);

        init();

        return view;
    }

    void init() {
        Snackbar.make(getActivity().findViewById(android.R.id.content), "Ürün listesi getirildi", Snackbar.LENGTH_LONG)
                .show();
        rvProducts = (RecyclerView) view.findViewById(R.id.rv_products);
    }
}
